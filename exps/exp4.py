import hashlib

def hash(password):
    result = hashlib.sha512(password.encode())
    return result.hexdigest()

def bruteforce(guesspasswordlist, actual_password_hash):
    for guess_password in guesspasswordlist:
        if hash(guess_password) == actual_password_hash:
            print("Hey! your password is:", guess_password,
                  "\n please change this, it was really easy to guess it (:")
            # If the password is found then it will terminate the script here
            exit()

actual_password = 'student'
actual_password_hash = hash(actual_password)
print(actual_password_hash)
guesspasswordlist = ['heslo1', '1234','student','henry']

bruteforce(guesspasswordlist, actual_password_hash)





